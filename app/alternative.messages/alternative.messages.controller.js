define([], function() {
    return ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
        $scope.allMessages = [];  // object to store messages

        $scope.removeMessage = function(message){
            $timeout(function(){
                $scope.allMessages.splice($scope.allMessages.indexOf(message),1);
            },message.lifetime);
        };

        $scope.getMessages = function(source){
            if (!$scope.allMessages.length){
                $http.get(source).success(function(result){
                    var base64RegExp = /\/[0-9a-zA-Z\+/=]{20,}/,
                        base64Data = '',
                        base64ImageData = '';
                    for (var item in result){  // get base64 data from plane text
                        base64Data = result[item].text.match(base64RegExp);
                        if (base64Data && base64Data != 'null'){
                            base64ImageData = base64ImageData.concat(base64Data);
                            result[item].image = base64ImageData; // store image data
                            result[item].text = result[item].text.replace(base64ImageData,''); // remove base64 data from text
                        }
                    }
                    $scope.allMessages = result;
                });
            }
        };

        $scope.resolveMessageImageType = function(message){ //check for the image type [base64 string || url]
            var urlRegExp = /^(http|https|ftp)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/;
            if (message.image.match(urlRegExp)){
                return message.image;
            } else {
                return 'data:image/jpeg;base64,'+message.image;
            }
        };

        $scope.getMessages('api/messages.json');

        $scope.$apply();
    }];
});
