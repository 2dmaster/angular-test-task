'use strict';

define(['angular', 'app'], function(angular, app) {

	return app.config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/messages.v1', {
            templateUrl: 'app/messages/messages.view.html',
            controller: 'messagesCtrl'
        });
        $routeProvider.when('/messages.v2', {
            templateUrl: 'app/alternative.messages/alternative.messages.view.html',
            controller: 'altMessagesCtrl'
        });
		$routeProvider.otherwise({redirectTo: '/messages.v1'});
	}]);

});
