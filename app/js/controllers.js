'use strict';

define(['angular', 'services'], function (angular) {

	/* Controllers */
	
	return angular.module('myApp.controllers', ['myApp.services'])
		// Sample controller where service is being used
		.controller('globalCtrl', ['$scope', 'version', function ($scope, version) {
			$scope.scopedAppVersion = version;
            $scope.menuItems = [ // main menu object
                {
                    title:'Messages - Directives way',
                    hash:'#messages.v1'
                },
                {
                    title:'Messages - Controller way',
                    hash:'#messages.v2'
                }
            ];

            $scope.setActivePage = function($event){
                $scope.activePage = $event.target.hash;
            };

            $scope.activePage = $scope.menuItems[0].hash;
		}])
		// Controllers from an external files
		.controller('messagesCtrl', ['$scope', '$injector', function($scope, $injector) {
			require(['../messages/messages.controller'], function(messagesCtrl) {
				$injector.invoke(messagesCtrl, this, {'$scope': $scope});
			});
		}])
        .controller('altMessagesCtrl', ['$scope', '$injector', function($scope, $injector) {
            require(['../alternative.messages/alternative.messages.controller'], function(altMessagesCtrl) {
                $injector.invoke(altMessagesCtrl, this, {'$scope': $scope});
            });
        }]);
});
