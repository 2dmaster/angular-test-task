'use strict';

define(['angular', 'services'], function (angular, services) {

	/* Filters */
	
	angular.module('myApp.filters', ['myApp.services'])
		.filter('interpolate', ['version', function(version) {
			return function(text) {
				return String(text).replace(/\%VERSION\%/mg, version);
			};
	    }])
        .filter('inSeconds', ['$interval', function($interval){
            return function(time){
                return time/1000;
            }
        }])
    ;
});
