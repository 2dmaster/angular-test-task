'use strict';

define(['angular', 'services'], function(angular, services) {
	
	angular.module('myApp.directives', ['myApp.services'])
		.directive('appVersion', ['version', function(version) {
			return function(scope, element, attrs) {
				element.text(version);
		    };
	    }])
        .directive('lifetime', function($interval, $timeout){  // display message lifetime directive
            return {
                restrict:'E',
                scope:{
                    value:'='
                },
                link:function($scope){
                    $timeout(function(){
                        $scope.lifeTime = $scope.value/1000;
                        $interval(function(){
                            if ($scope.lifeTime > 0){
                                $scope.lifeTime--;
                            }
                        }, 1000);
                    });
                },
                template:
                    '<span>{{lifeTime}}</span>'
            }
        })
        .directive('bindEvent', function ($timeout) { // bind expire event when message item rendered
            return {
                restrict:'A',
                scope:{
                    onMessageExpire:'&'
                },
                link: function ($scope) {
                    $timeout(function () {
                        $scope.onMessageExpire();
                    });
                }
            }
        })
        .directive('messagesList',['$messageService', '$timeout', function($messageService, $timeout){
            // $messageService - service that pulls messages from the server
            return {
                restrict : 'E',
                scope:{
                    messagesSourceUrl:'@'
                },
                link : function($scope){

                    $scope.allMessages = [];  // object to store messages

                    $scope.removeMessage = function(message){
                        $timeout(function(){
                            $scope.allMessages.splice($scope.allMessages.indexOf(message),1);
                        },message.lifetime);
                    };

                    $scope.getMessages = function(){
                        if (!$scope.allMessages.length){
                            $messageService.getMessages($scope.messagesSourceUrl, function(result){
                                $scope.allMessages = result;
                            });
                        }
                    };

                    $scope.resolveMessageImageType = function(message){ //check for the image type [base64 string || url]
                        var urlRegExp = /^(http|https|ftp)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?\/?([a-zA-Z0-9\-\._\?\,\'\/\\\+&amp;%\$#\=~])*$/;
                        if (message.image.match(urlRegExp)){
                            return message.image;
                        } else {
                            return 'data:image/jpeg;base64,'+message.image;
                        }
                    };

                    $scope.getMessages();
                },
                template:
                    '<div class="row">' +
                        '<div class="col-md-12">' +
                            '<ul class="messages-list">' +
                                '<li class="messages-list__message" ng-repeat="message in allMessages track by $index" bind-event on-message-expire="removeMessage(message)">' +
                                    '<div class="row">' +
                                        '<div class="col-md-12">' +
                                            '<img class="message-image pull-left" ng-src="{{resolveMessageImageType(message)}}">'+
                                            '<span class="message-text">{{message.text}}</span>' +
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row">' +
                                        '<div class="col-md-12">' +
                                            '<span class="pull-right">Message lifetime: <lifetime value="message.lifetime"></lifetime> sec.</span>' +
                                        '</div>'+
                                    '</div>'+
                                '</li>'+
                            '</ul>'+
                        '</div>'+
                    '</div>'+
                    '<div class="row">' +
                        '<div class="col-md-12">' +
                            '<div class="btn btn-primary pull-right" ng-class="{disabled: allMessages.length}" ng-click="getMessages()">Reload messages</div>'+
                        '</div>'+
                    '</div>'
            }
        }])
    ;
});
