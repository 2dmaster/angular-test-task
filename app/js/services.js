'use strict';

define(['angular'], function (angular) {
	/* Services */
	angular.module('myApp.services', [])
        .value('version','1.0')
        .service('$activePage',function($location){
            this.currentPage = $location.$$hash;
        })
		.service('$messageService', function($http){  // service that pulls messages from the server
            var base64RegExp = /\/[0-9a-zA-Z\+/=]{20,}/;
            this.getMessages = function(apiEndPoint, cb){
                $http.get(apiEndPoint)
                    .success(function(result){
                        var base64Data = '',
                            base64ImageData = '';
                        for (var item in result){  // get base64 data from plane text
                            base64Data = result[item].text.match(base64RegExp);
                            if (base64Data && base64Data != 'null'){
                                base64ImageData = base64ImageData.concat(base64Data);
                                result[item].image = base64ImageData; // store image data
                                result[item].text = result[item].text.replace(base64ImageData,''); // remove base64 data from text
                            }
                        }
                        cb(result);
                    });
            }
        })
    ;
});
